
Dropbox API


App will connect to a dropbox account and displays some results


Requirements
------------------
1) Authenticating the user by giving access key and secret key
2) Getting user information 
3) Showing the folders and files of given directory


Technology
------------------
Java, Need Dropbox SDK


HowTo
------------------
1) App Name is stored in src/main/resources/config.properties - update this to your app name
2) For unit testing a working dropbox account is needed. The details need to be entered in src/test/resources/config.properties
- Both config.properties are straight forward to understand
3) Package with maven and run the app	
4) Help is available when you run the app without arguments OR use the help switch
- java -jar <packaged-dropbox-app.jar> help