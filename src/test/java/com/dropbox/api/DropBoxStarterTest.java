package com.dropbox.api;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.dropbox.core.DbxApiException;
import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;

@ExtendWith(MockitoExtension.class)
public class DropBoxStarterTest {

	private String appKey;
	private String secretKey;
	private String accessToken;
	private String path;
	private String locale;


	@Mock
	private Authentication authenticationMock;

	@Mock
	private UserInfo userInfoMock;

	@Mock
	private DirectoryInfo dirInfoMock;

	@Mock
	private DbxClientV2 clientMock;

	@InjectMocks
	private DropBoxStarter dropBoxStarterMock;

	@Test
	public void callAuthenticationTest() throws IOException {
		Mockito.doReturn(true).when(authenticationMock).doAuthentication(appKey, secretKey);
		dropBoxStarterMock.callAuthentication(appKey, secretKey);
		verify(authenticationMock, times(1)).doAuthentication(appKey, secretKey);
	}

	@Test
	public void displayUserInfoTest() throws DbxApiException, DbxException, IOException {
		Mockito.doReturn(true).when(userInfoMock).getUserInfo(accessToken, locale);
		dropBoxStarterMock.displayUserInfo(accessToken, locale);
		verify(userInfoMock, times(1)).getUserInfo(accessToken, locale);
	}

	@Test
	public void getDirectoryInfoTest() throws DbxApiException, DbxException, IOException {
		Mockito.doReturn(true).when(dirInfoMock).getFolderAndFileInfo(accessToken, path, locale);
		dropBoxStarterMock.getDirectoryInfo(accessToken, path, locale);
		verify(dirInfoMock, times(1)).getFolderAndFileInfo(accessToken, path, locale);
	}
}
