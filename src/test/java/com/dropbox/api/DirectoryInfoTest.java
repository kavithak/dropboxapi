package com.dropbox.api;

import static org.junit.jupiter.api.Assertions.assertSame;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.dropbox.core.DbxApiException;
import com.dropbox.core.DbxException;

@ExtendWith(MockitoExtension.class)
public class DirectoryInfoTest {
	private static Properties properties = new Properties();

	@BeforeAll
	public static void setup() throws IOException {
		// Load test credentials from config.properties
		InputStream input = UserInfo.class.getClassLoader().getResourceAsStream("config.properties");
		properties.load(input);
	}

	@InjectMocks
	private DirectoryInfo dirInfo;

	@Test
	public void doAuthenticationTest() throws DbxApiException, DbxException, IOException {
		boolean actual = dirInfo.getFolderAndFileInfo(properties.getProperty("DROP_BOX_ACCESS_TOKEN"), "", "EN");
		assertSame(true, actual);
		assertSame(true, dirInfo.isOkay());
	}

	@Test
	public void doAuthenticationTestFail() throws DbxApiException, DbxException, IOException {
		boolean actual = dirInfo.getFolderAndFileInfo("XX", "", "EN");
		assertSame(false, actual);
		assertSame(false, dirInfo.isOkay());
	}

	@Test
	public void doFolderTest1() throws DbxApiException, DbxException, IOException {
		boolean actual = dirInfo.getFolderAndFileInfo(properties.getProperty("DROP_BOX_ACCESS_TOKEN"), "", "EN");
		assertSame(true, actual);
		assertSame(true, dirInfo.isOkay());
		assertSame(true, !dirInfo.getFolderMap().isEmpty());
	}

	@Test
	public void doFolderTest2() throws DbxApiException, DbxException, IOException {
		boolean actual = dirInfo.getFolderAndFileInfo(properties.getProperty("DROP_BOX_ACCESS_TOKEN"),
				properties.getProperty("TEST_FOLDER1"), "EN");
		assertSame(true, actual);
		assertSame(true, dirInfo.isOkay());
		assertSame(true, !dirInfo.getFolderMap().isEmpty());
		assertSame(true, !dirInfo.getFileMap().isEmpty());
	}


	@Test
	public void doFolderTest3() throws DbxApiException, DbxException, IOException {
		boolean actual = dirInfo.getFolderAndFileInfo(properties.getProperty("DROP_BOX_ACCESS_TOKEN"),
				properties.getProperty("TEST_FOLDER2"), "EN");
		assertSame(true, actual);
		assertSame(true, dirInfo.isOkay());
		assertSame(false, !dirInfo.getFolderMap().isEmpty()); // there are no folders under TEST_FOLDER2
		assertSame(true, !dirInfo.getFileMap().isEmpty());
	}
	
	@Test
	public void doFolderFail1() throws DbxApiException, DbxException, IOException {
		boolean actual = dirInfo.getFolderAndFileInfo(properties.getProperty("DROP_BOX_ACCESS_TOKEN"),
				properties.getProperty("TEST_FOLDER_FAIL1"), "EN");
		assertSame(false, actual);
		assertSame(false, dirInfo.isOkay());
		assertSame(true, dirInfo.getFolderMap().isEmpty());
		assertSame(true, dirInfo.getFileMap().isEmpty());
	}
	
	@Test
	public void doFolderFail2() throws DbxApiException, DbxException, IOException {
		boolean actual = dirInfo.getFolderAndFileInfo(properties.getProperty("DROP_BOX_ACCESS_TOKEN"),
				properties.getProperty("TEST_FOLDER_FAIL2"), "EN");
		assertSame(false, actual);
		assertSame(false, dirInfo.isOkay());
		assertSame(true, dirInfo.getFolderMap().isEmpty());
		assertSame(true, dirInfo.getFileMap().isEmpty());
	}
	
	@Disabled
	@Test
	public void getFolderAndFileInfoTest() throws DbxApiException, DbxException, IOException {

		dirInfo.getFolderAndFileInfo(properties.getProperty("DROP_BOX_ACCESS_TOKEN"),
				properties.getProperty("TEST_FOLDER1"), "en");
	}

}
