package com.dropbox.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DropBoxCommandsTest {

	private static Properties properties = new Properties();

	@BeforeAll
	public static void setup() throws IOException {
		// Load test credentials from config.properties
		InputStream input = UserInfo.class.getClassLoader().getResourceAsStream("config.properties");
		properties.load(input);
	}

	@InjectMocks
	private DropBoxCommands dropBoxCommandsMock;


	@Test
	public void parseCommandsInputIsEmpty() {
		String noargs[]  = new String[0];
		boolean actual = dropBoxCommandsMock.parseCommand(Arrays.asList(noargs));
		assertSame(false, actual);
	}

	@Test
	public void parseCommandsInputIsHelp() {
		String noargs[]  = new String[] {"help"};
		boolean actual = dropBoxCommandsMock.parseCommand(Arrays.asList(noargs));
		assertSame(false, actual, "\"Supported commands: auth, info, list \\n Do <command> help for further help\"");
	}

	@Test
	public void parseCommandsAuthOkay() {
		String noargs[]  = new String[] {"auth", "key", "secret"};
		boolean actual = dropBoxCommandsMock.parseCommand(Arrays.asList(noargs));
		assertSame(true, actual);
	}
	
	@Test
	public void parseCommandsAuthWithHelp() {
		// 2nd arg help should display help
		String noargs[]  = new String[] {"auth", "help", "secret"};
		boolean actual = dropBoxCommandsMock.parseCommand(Arrays.asList(noargs));
		assertSame(false, actual);
	}
	
	@Test
	public void parseCommandsAuthNotOkay() {
		String noargs[]  = new String[] {"auth", "secret"};
		boolean actual = dropBoxCommandsMock.parseCommand(Arrays.asList(noargs));
		assertSame(false, actual);
	}
	
	@Test
	public void parseCommandsInfoOkay() {
		String noargs[]  = new String[] {"info", "token"};
		boolean actual = dropBoxCommandsMock.parseCommand(Arrays.asList(noargs));
		assertSame(true, actual);
	}
	
	@Test
	public void parseCommandsInfoWithLocaleOkay() {
		String noargs[]  = new String[] {"info", "token", "locale"};
		boolean actual = dropBoxCommandsMock.parseCommand(Arrays.asList(noargs));
		assertSame(true, actual);
	}
	
	@Test
	public void parseCommandsInfoWithHelp() {
		// 2nd arg help should display help
		String noargs[]  = new String[] {"info", "help", "locale"};
		boolean actual = dropBoxCommandsMock.parseCommand(Arrays.asList(noargs));
		assertSame(false, actual);
	}
	
	@Test
	public void parseCommandsInfoNotOkay() {
		String noargs[]  = new String[] {"info"};
		boolean actual = dropBoxCommandsMock.parseCommand(Arrays.asList(noargs));
		assertSame(false, actual);
	}
	
	@Test
	public void parseCommandsListOkay() {
		String noargs[]  = new String[] {"list", "token", "path"};
		boolean actual = dropBoxCommandsMock.parseCommand(Arrays.asList(noargs));
		assertSame(true, actual);
	}
	
	@Test
	public void parseCommandsListWithHelp() {
		// 2nd arg help should display help
		String noargs[]  = new String[] {"list", "help", "path"};
		boolean actual = dropBoxCommandsMock.parseCommand(Arrays.asList(noargs));
		assertSame(false, actual);
		assertSame(false, dropBoxCommandsMock.isOkay());
	}
	
	@Test
	public void parseCommandsListWithHelp2() {
		// 3rd arg help is taken as normal
		String noargs[]  = new String[] {"list", "path", "help"};
		boolean actual = dropBoxCommandsMock.parseCommand(Arrays.asList(noargs));
		assertSame(true, actual);
		assertSame(true, dropBoxCommandsMock.isOkay());
	}
	
	
	@Test
	public void parseCommandsListWithLocaleOkay() {
		String noargs[]  = new String[] {"list", "token", "path", "locale"};
		boolean actual = dropBoxCommandsMock.parseCommand(Arrays.asList(noargs));
		assertSame(true, actual);
	}
	
	@Test
	public void parseCommandsListMissingPath() {
		String noargs[]  = new String[] {"list", "token"};
		boolean actual = dropBoxCommandsMock.parseCommand(Arrays.asList(noargs));
		assertSame(false, actual);
	}
	
	@Test
	public void parseCommandsAuthOkayExtended() {
		String noargs[]  = new String[] {"auth", "key", "secret"};
		boolean actual = dropBoxCommandsMock.parseCommand(Arrays.asList(noargs));
		assertSame(true, actual);
		//assertSame(true, !dropBoxCommandsMock.getAccessToken().isEmpty());
		//assertEquals(properties.getProperty("DROP_BOX_APP_KEY"), dropBoxCommandsMock.getAppKey());;

	}
}
