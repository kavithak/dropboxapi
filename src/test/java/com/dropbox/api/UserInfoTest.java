package com.dropbox.api;

import static org.junit.jupiter.api.Assertions.assertSame;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.dropbox.core.DbxApiException;
import com.dropbox.core.DbxException;

@ExtendWith(MockitoExtension.class)
public class UserInfoTest {
	private static Properties properties = new Properties();

	@BeforeAll
	public static void setup() throws IOException {
		// Load test credentials from config.properties
		InputStream input = UserInfo.class.getClassLoader().getResourceAsStream("config.properties");
		properties.load(input);
	}

	@InjectMocks
	private UserInfo userInfo;

	@Test
	public void doAuthenticationTest() throws DbxApiException, DbxException, IOException {
		boolean actual = userInfo.getUserInfo(properties.getProperty("DROP_BOX_ACCESS_TOKEN"), "EN");
		assertSame(true, actual);
		assertSame(true, userInfo.isOkay());
	}

	@Test
	public void doAuthenticationTestFail() throws DbxApiException, DbxException, IOException {
		boolean actual = userInfo.getUserInfo("xxx", "EN");
		assertSame(false, actual);
		assertSame(false, userInfo.isOkay());
	}

	@Test
	public void doAuthenticationTestFailExtended() throws DbxApiException, DbxException, IOException {
		boolean actual = userInfo.getUserInfo("yyy", "EN");
		assertSame(false, actual);
		assertSame(false, userInfo.isOkay());
		assertSame("InvalidId", userInfo.getUserId());
		assertSame("InvalidName", userInfo.getDisplayName());
		assertSame("InvalidName", userInfo.getName());
		assertSame("InvalidEmail", userInfo.getEmail());
		assertSame("InvalidCountry", userInfo.getCountry());
		assertSame("InvalidReferalLink", userInfo.getReferalLink());
	}

	@Disabled
	@Test
	public void getUserInfoTest() throws DbxApiException, DbxException, IOException {
		UserInfo userInfo = new UserInfo();
		userInfo.getUserInfo(properties.getProperty("DROP_BOX_ACCESS_TOKEN"), Locale.getDefault().toString());
		// assertEquals(country, userInfo.getAccount().getCountry());
	}

}
