package com.dropbox.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class AuthenticationTest {
	private static Properties properties = new Properties();

	@BeforeAll
	public static void setup() throws IOException {
		// Load test credentials from config.properties
		InputStream input = UserInfo.class.getClassLoader().getResourceAsStream("config.properties");
		properties.load(input);
	}

	@Test
	public void doAuthenticationTest() throws IOException {
		Authentication authentication = new Authentication();
		boolean actual = authentication.doAuthentication(properties.getProperty("DROP_BOX_APP_KEY"),
				properties.getProperty("DROP_BOX_APP_SECRET"));
		assertSame(true, actual);
		assertSame(true, authentication.isOkay());
		assertEquals(properties.getProperty("USER_ID"), authentication.getAuthFinish().getUserId());
		assertSame(true, !authentication.getAccessToken().isEmpty());
	}

	// @Disabled
	@Test
	public void doAuthenticationTestInvalidCredentials() throws IOException {
		Authentication authentication = new Authentication();
		boolean actual = authentication.doAuthentication("xxx", properties.getProperty("DROP_BOX_APP_SECRET"));
		assertSame(false, authentication.isOkay());
		assertSame(false, actual);
		assertSame(true, authentication.getAccessToken().isEmpty());

	}
}
