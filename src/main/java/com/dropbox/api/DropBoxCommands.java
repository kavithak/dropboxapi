package com.dropbox.api;

import java.util.List;
import java.util.Locale;

/**
 * Not using Apache CLI or other command line parsers Given task has only 3
 * commands to support
 *
 */
public class DropBoxCommands {

	private String accessToken;
	private String appKey;
	private String appSecret;
	private String locale;
	private String path;
	private String command;
	private boolean okay;

	final private String help = "Supported commands: auth, info, list \n Do <command> help for further help";
	final private String authHelp = "auth {appKey} {appSecret} :: authenticate and authorize "
			+ "Dropbox user resulting in the authorization code for other commands";
	final private String infoHelp = "info {accessToken} {locale} :: print the user information."
			+ " accessToken could be generated with auth command. locale(optional) is the users locale.";
	final private String listHelp = "list {accessToken} {path} :: prints the list of files and folders "
			+ "within the specified directory";

	// parse commands return true if successfully parsed.
	public boolean parseCommand(List<String> inputs) {
		okay = false;
		if (inputs.isEmpty()) {
			System.out.println(help);
			return false;
		}

		setCommand(inputs.get(0));

		if (command.equalsIgnoreCase("help") || inputs.size() < 2) {
			System.out.println(help);
			return false;
		}

		boolean ok = (command.equalsIgnoreCase("auth") || command.equalsIgnoreCase("info")
				|| command.equalsIgnoreCase("list"));
		if (!ok) {
			System.err.println("Unsupported command!");
			System.out.println(help);
			return false;
		}

		if (command.equalsIgnoreCase("auth")) {
			okay = parseCommandAuth(inputs);
		} else if (command.equalsIgnoreCase("info")) {
			okay = parseCommandInfo(inputs);
		} else if (command.equalsIgnoreCase("list")) {
			okay = parseCommandList(inputs);
		}

		return okay;
	}

	public boolean isOkay() {
		return okay;
	}

	private boolean parseCommandAuth(List<String> inputs) {
		if ((inputs.size() > 1) && inputs.get(1).equalsIgnoreCase("help")) {
			System.out.println(authHelp);
			return false;
		}

		if (inputs.size() < 3) {
			System.err.println("Incorrect command usage!");
			System.out.println(authHelp);
			return false;
		}

		setAppKey(inputs.get(1));
		setAppSecret(inputs.get(2));
		return true; // remaining if any simply ignore
	}

	private boolean parseCommandInfo(List<String> inputs) {
		if (inputs.size() < 2) {
			System.err.println("Incorrect command usage!");
			System.out.println(infoHelp);
			return false;
		}

		if (inputs.get(1).equalsIgnoreCase("help")) {
			System.out.println(infoHelp);
			return false;
		}

		setAccessToken(inputs.get(1));
		if (inputs.size() > 2) {
			setLocale(inputs.get(2));
		} else {
			setLocale(Locale.getDefault().toString());
		}
		return true; // remaining if any simply ignore
	}

	private boolean parseCommandList(List<String> inputs) {
		if ((inputs.size() > 1) && inputs.get(1).equalsIgnoreCase("help")) {
			System.out.println(listHelp);
			return false;
		}

		if (inputs.size() < 3) {
			System.err.println("Incorrect command usage!");
			System.out.println(listHelp);
			return false;
		}

		setAccessToken(inputs.get(1));
		setPath(inputs.get(2));
		if (inputs.size() > 3) {
			setLocale(inputs.get(3));
		} else {
			setLocale(Locale.getDefault().toString());
		}
		return true; // remaining if any simply ignore
	}

	public String getAccessToken() {
		return accessToken;
	}

	private void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getAppKey() {
		return appKey;
	}

	private void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	private void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public String getLocale() {
		return locale;
	}

	private void setLocale(String locale) {
		this.locale = locale;
	}

	public String getPath() {
		return path;
	}

	private void setPath(String path) {
		this.path = path;
	}

	public String getCommand() {
		return command;
	}

	private void setCommand(String command) {
		this.command = command;
	}

}
