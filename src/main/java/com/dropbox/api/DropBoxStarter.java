package com.dropbox.api;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import com.dropbox.core.DbxApiException;
import com.dropbox.core.DbxException;

public class DropBoxStarter {

	// @Inject
	private Authentication authentication = new Authentication();

	private UserInfo userInfo = new UserInfo();

	private DirectoryInfo dirInfo = new DirectoryInfo();

	public static void main(String[] args) throws IOException, DbxApiException, DbxException {
		DropBoxCommands commands = new DropBoxCommands();
		commands.parseCommand(Arrays.asList(args));
		if (!commands.isOkay()) {
			return;
		}

		DropBoxStarter starter = new DropBoxStarter();
		if (commands.getCommand().equalsIgnoreCase("auth")) {
			starter.callAuthentication(commands.getAppKey(), commands.getAppSecret());
		} else if (commands.getCommand().equalsIgnoreCase("info")) {
			starter.displayUserInfo(commands.getAccessToken(), commands.getLocale());
		} else if (commands.getCommand().equalsIgnoreCase("list")) {
			starter.getDirectoryInfo(commands.getAccessToken(), commands.getPath(), commands.getLocale());
		}

	}

	public void callAuthentication(String appKey, String secretKey) throws IOException {
		boolean result = authentication.doAuthentication(appKey, secretKey);
		if (result) {
			System.out.println("Your access token: " + authentication.getAccessToken());
		}
	}

	public void displayUserInfo(String token, String locale) throws DbxApiException, DbxException, IOException {
		userInfo.getUserInfo(token, locale);
		if (userInfo.isOkay()) {
			System.out.println("----------------------------------------------");
			System.out.println("User ID:        " + userInfo.getUserId());
			System.out.println("Display name:   " + userInfo.getDisplayName());
			System.out.println("Name:           " + userInfo.getName());
			System.out.println("EMail:          " + userInfo.getEmail());
			System.out.println("Country:        " + userInfo.getCountry());
			System.out.println("Referral Link:  " + userInfo.getReferalLink());
			System.out.println("----------------------------------------------");
		}
	}

	public void getDirectoryInfo(String token, String path, String locale)
			throws DbxApiException, DbxException, IOException {
		dirInfo.getFolderAndFileInfo(token, path, locale);
		if (!dirInfo.isOkay())
			return;

		System.out.println("----------------------------------------------------------");
		System.out.println(path);
		Map<String, String> folderMap = dirInfo.getFolderMap();
		for (Map.Entry<String, String> entry : folderMap.entrySet()) {
			String folder = entry.getKey();
			String details = entry.getValue();
			System.out.println(" - " + folder + " : " + details);
		}

		Map<String, String> fileMap = dirInfo.getFileMap();
		for (Map.Entry<String, String> entry : fileMap.entrySet()) {
			String file = entry.getKey();
			String details = entry.getValue();
			System.out.println(" - " + file + " : " + details);
		}

		System.out.println("----------------------------------------------------------");

	}

}
