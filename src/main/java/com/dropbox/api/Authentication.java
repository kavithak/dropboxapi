package com.dropbox.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuth;

public class Authentication {
	private DbxAuthFinish authFinish;
	private boolean okay = false;

	public boolean doAuthentication(String appKey, String secretKey) throws IOException {
		Properties properties = new Properties();
		DbxRequestConfig requestConfig;
		DbxAppInfo dbxAppInfo;
		try {
			// Load app.name from config.properties
			InputStream input = UserInfo.class.getClassLoader().getResourceAsStream("config.properties");
			properties.load(input);
			dbxAppInfo = new DbxAppInfo(appKey, secretKey);
			requestConfig = DbxRequestConfig.newBuilder(properties.getProperty("app.name")).build();
		} catch (Exception e) {
			System.err.println("Something is wrong.. Please check invalid appKey/appSecret, network error etc?");
			okay = false;
			return okay;
		}

		DbxWebAuth webAuth = new DbxWebAuth(requestConfig, dbxAppInfo);
		DbxWebAuth.Request webAuthRequest = DbxWebAuth.newRequestBuilder().withNoRedirect().build();
		String authorizeUrl = webAuth.authorize(webAuthRequest);

		System.out.println("1. Go to " + authorizeUrl);
		System.out.println("2. Click \"Allow\" (you might have to log in first).");
		System.out.println("3. Copy the authorization code.");
		System.out.print("Enter the authorization code here: ");
		String code = new BufferedReader(new InputStreamReader(System.in)).readLine();
		code = code.trim();
		try {
			authFinish = webAuth.finishFromCode(code);
		} catch (DbxException ex) {
			System.out.println("Authentication failed, Provided code is not valid");
			okay = false;
			return okay;
		}
		setAuthFinish(authFinish);
		okay = true;
		return okay;
	}

	public DbxAuthFinish getAuthFinish() {
		return authFinish;
	}

	public boolean isOkay() {
		return okay;
	}

	public String getAccessToken() {
		if (!isOkay())
			return "";
		return authFinish.getAccessToken();
	}

	private void setAuthFinish(DbxAuthFinish authFinish) {
		this.authFinish = authFinish;
	}

}
