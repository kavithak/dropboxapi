package com.dropbox.api;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.LinkedHashMap;
import java.util.Map;

import com.dropbox.core.DbxApiException;
import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.FolderMetadata;
import com.dropbox.core.v2.files.ListFolderErrorException;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;

public class DirectoryInfo {

	private boolean okay = false;
	private Map<String, String> folderMap;
	private Map<String, String> fileMap;

	public boolean getFolderAndFileInfo(String token, String path, String locale)
			throws DbxApiException, DbxException, IOException {

		folderMap = new LinkedHashMap<String, String>();
		fileMap = new LinkedHashMap<String, String>();

		UserInfo userInfo = new UserInfo();
		if (!userInfo.getUserInfo(token, locale)) {
			okay = false;
			return okay;
		}

		try {
			DbxClientV2 client = userInfo.getClient();
			ListFolderResult result = client.files().listFolder(path);
			while (true) {
				for (Metadata metadata : result.getEntries()) {
					if (metadata instanceof FolderMetadata) {
						final FolderMetadata folderMetadata = (FolderMetadata) metadata;
						folderMap.put(folderMetadata.getName(), "dir");
						// TODO: put more details for folders here if needed
						// System.out.println(folderMetadata.getPathDisplay());
					} else if (metadata instanceof FileMetadata) {
						final FileMetadata fileMetadata = (FileMetadata) metadata;
						final ZoneId zoneId = ZoneOffset.UTC;
						final Instant clientModifiedInstant = fileMetadata.getClientModified().toInstant();
						// final Instant serverModifiedInstant =
						// fileMetadata.getServerModified().toInstant();
						// TODO: get more file details here
						final String details = "file, " + String.valueOf(fileMetadata.getSize()) + " "
								+ LocalDateTime.ofInstant(clientModifiedInstant, zoneId);
						fileMap.put(fileMetadata.getName(), details);
					}
				}

				if (!result.getHasMore()) {
					break;
				}
				result = client.files().listFolderContinue(result.getCursor());
			}
		} catch (ListFolderErrorException e) {
			System.err.println(" Given Folder does not exist!");
			okay = false;
			return okay;
		} catch (Exception e) {
			System.err.println("Something is wrong.. Please check access token, network error etc");
			okay = false;
			return okay;
		}

		okay = true;
		return okay;
	}

	public boolean isOkay() {
		return okay;
	}

	// gets the folder information as a map
	public Map<String, String> getFolderMap() {
		return folderMap;
	}

	// gets the file information as a map
	public Map<String, String> getFileMap() {
		return fileMap;
	}

}
