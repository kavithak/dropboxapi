package com.dropbox.api;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.dropbox.core.DbxApiException;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.users.FullAccount;

public class UserInfo {

	private FullAccount account;
	private DbxClientV2 client;
	private boolean okay = false;

	public boolean getUserInfo(String accessToken, String locale) throws DbxApiException, DbxException, IOException {
		Properties properties = new Properties();

		try {
			InputStream input = UserInfo.class.getClassLoader().getResourceAsStream("config.properties");
			properties.load(input);
			DbxRequestConfig requestConfig = DbxRequestConfig.newBuilder(properties.getProperty("app.name"))
					.withUserLocale(locale.toString()).build();
			client = new DbxClientV2(requestConfig, accessToken);
			account = client.users().getCurrentAccount();
		} catch (Exception e) {
			System.err.println("Something is wrong.. Please check access token, network error etc?");
			okay = false;
			return okay;
		}

		okay = true;
		return okay;
	}

	// needed for directory listing
	public DbxClientV2 getClient() {
		return client;
	}

	public boolean isOkay() {
		return okay;
	}

	public String getUserId() {
		if (!isOkay())
			return "InvalidId";
		return account.getAccountId();
	}

	public String getDisplayName() {
		if (!isOkay())
			return "InvalidName";
		return account.getName().getDisplayName();
	}

	public String getName() {
		if (!isOkay())
			return "InvalidName";
		return account.getName().getGivenName() + " " + account.getName().getSurname()
				+ " (" + account.getName().getFamiliarName() + ")";
	}

	public String getEmail() {
		if (!isOkay())
			return "InvalidEmail";
		String verifiedStr = account.getEmailVerified() ? "(Verified)" : "(Not verified)";
		return account.getEmail() + " " + verifiedStr;
	}

	public String getCountry() {
		if (!isOkay())
			return "InvalidCountry";
		return account.getCountry();
	}

	public String getReferalLink() {
		if (!isOkay())
			return "InvalidReferalLink";
		return account.getReferralLink();
	}

}
